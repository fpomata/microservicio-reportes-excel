import json
from pathlib import Path
import pandas as pd
from tools import db_connect, send_email


PATH_REPORTES = Path(__file__).parent / 'archivos/'
CFILE = Path(__file__).parent / 'config.json'

def read_data(sql_object):
    """
    Se lee el objeto sql (tabla o vista) y se retorna un DataFrame de la consulta SELECT.
    @param: sql_object
    @type: str
    """
    engine = db_connect()
    engine.execute("USE DWH")
    query = f"select * from {sql_object};"
    df = pd.read_sql(query, engine)

    return df

def load_data(df, file_path, file_name):
    """
    Se genera el archivo Excel para el reporte a partir del DataFrame.
    @param: df
    @type: Pandas DataFrame
    @param: file_path
    @type: str
    @param: file_name
    @type: str
    """
    from pathlib import Path
    try:
        data_folder = Path(file_path)
        file_to_save = data_folder / file_name
        df.to_excel(file_to_save, index=False)
        print(f'Archivo Excel {file_name} creado correctamente.')
    except Exception as e:
        raise Exception('Error al crear el archivo Excel.', e)

def generar_reporte(receptores, nombre_reporte = 'Control cargas ES'):
    """
    Se genera reporte Excel y se envia por email.
    @param: receptores
    @type: list[str]
    @param: nombre_reporte
    @type: str
    """
    with open(CFILE) as config_file:
        config_parsed = json.load(config_file)
        reportes = config_parsed["reportes"]

        # verificar si el nombre del reporte que llega desde la API corresponde al valor en
        # el archivo config.json
        if (nombre_reporte in reportes):
            sql_object = reportes[nombre_reporte]
        else:
            raise Exception(f'Nombre de reporte erroneo ({nombre_reporte}).')

    # se renombra para evitar problemas con el nombre del archivo
    nombre_reporte = nombre_reporte.replace(' ', '_').strip()

    try:
        dataframe = read_data(sql_object)
        load_data(dataframe, PATH_REPORTES, f'{nombre_reporte}.xlsx')
        send_email(
            receptores, 
            f'{nombre_reporte}', 
            PATH_REPORTES
        )
    except Exception as e:
        raise Exception('Error al general el reporte en Excel', e)