import pyodbc
import urllib
from sqlalchemy import create_engine
from pathlib import Path
import json
import sys
import os
from redmail import outlook


global CFILE
CFILE = Path(__file__).parent / 'config.json'

def fast_connect():
    """Lee archivo de configuración json y crea una cadena de conexion
    @param configfile: path de archivo de configuración
    @type configfile: str
    """
    try:
        with open(CFILE) as connect_config_file:
            config_file = json.load(connect_config_file)

            server = config_file['server']
            uid = config_file['uid']
            pwd = config_file['pwd']
        #Arma cadena de conexión e intena conectarse
        if sys.platform == "win32":
            string = 'Driver={SQL Server};Server=%s;Database=STG;UID=%s;PWD=%s;' % (server, uid, pwd)
        elif sys.platform == "linux":
            string = 'Driver={/opt/microsoft/msodbcsql18/lib64/libmsodbcsql-18.0.so.1.1};Server=%s;Database=STG;UID=%s;PWD=%s;TrustServerCertificate=yes;' % (server, uid, pwd)
        else:
            print("No hay compatibilidad con MacOS")

        print(f'Archivo de configuracion {CFILE} parseado correctamente')
        return string

    except pyodbc.Error as ex:
        sqlstate = ex.args[1]
        print(f'Error al intentar conectar a la base de datos: {sqlstate}')
        raise Exception(sqlstate)

def db_connect():
    conn = urllib.parse.quote_plus(fast_connect()) 
    engine = create_engine(f"mssql+pyodbc:///?odbc_connect={conn}")

    return engine

def send_email(recipients, nombre_reporte, attachment_path):
    file_name = f'{nombre_reporte}.xlsx'
    full_path = f'{attachment_path}/{file_name}'

    with open(CFILE) as connect_config_file:
        config_file = json.load(connect_config_file)

        sender = config_file['email_sender']
        sender_password = config_file['email_sender_password']

    outlook.username = sender
    outlook.password = sender_password

    try:
        outlook.send(
            receivers=recipients,
            subject=f"Reporte de {nombre_reporte}",
            text="",
            attachments={
                file_name: Path(full_path)
            }
        )
        print(f'Reporte enviado exitosamente a {recipients}')
        delete_temp_files(full_path)
    except Exception as e:
        raise Exception('Error al enviar el correo.', e)

def delete_temp_files(path):
    if os.path.exists(path):
        os.remove(path)
    else:
        print(f"El archivo {path} no existe") 
