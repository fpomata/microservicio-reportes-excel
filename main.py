from fastapi import FastAPI
from pydantic import BaseModel
from typing import List
from procesar_reporte import generar_reporte


app = FastAPI()

class Item(BaseModel):
    nombre_reporte: str
    email: List[str] = []

@app.post("/reporte/")
async def create_reporte(item: Item):
    try:
        generar_reporte(item.email, item.nombre_reporte)
    except Exception as e:
        print('Error al enviar reporte', e)

    return f'Reporte de {item.nombre_reporte} enviado a: {item.email}'